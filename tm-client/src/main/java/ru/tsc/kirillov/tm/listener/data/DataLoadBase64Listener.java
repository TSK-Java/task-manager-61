package ru.tsc.kirillov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.DataBase64LoadRequest;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.event.ConsoleEvent;

@Component
public final class DataLoadBase64Listener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-base64";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Загрузить состояние приложения в формате Base64";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataLoadBase64Listener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Загрузить состояния приложения в формате Base64]");
        getDomainEndpoint().loadDataBase64(new DataBase64LoadRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
