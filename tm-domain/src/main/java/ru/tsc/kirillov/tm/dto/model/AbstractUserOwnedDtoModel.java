package ru.tsc.kirillov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractUserOwnedDtoModel extends AbstractDtoModel {

    @Nullable
    @Column(nullable = false, name = "user_id")
    private String userId;

    public AbstractUserOwnedDtoModel(@Nullable final String userId) {
        this.userId = userId;
    }

}
