package ru.tsc.kirillov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.TaskDto;

@NoArgsConstructor
public class TaskUpdateByIndexResponse extends AbstractTaskResponse {

    public TaskUpdateByIndexResponse(@Nullable final TaskDto task) {
        super(task);
    }

}
