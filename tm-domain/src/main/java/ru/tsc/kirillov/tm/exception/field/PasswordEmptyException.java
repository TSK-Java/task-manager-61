package ru.tsc.kirillov.tm.exception.field;

public final class PasswordEmptyException extends AbstractFieldException {

    public PasswordEmptyException() {
        super("Ошибка! Пароль не задан.");
    }

}
