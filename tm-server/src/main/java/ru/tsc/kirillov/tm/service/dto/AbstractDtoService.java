package ru.tsc.kirillov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.tsc.kirillov.tm.api.repository.dto.IDtoRepository;
import ru.tsc.kirillov.tm.api.service.dto.IDtoService;
import ru.tsc.kirillov.tm.dto.model.AbstractDtoModel;
import ru.tsc.kirillov.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kirillov.tm.exception.field.IdEmptyException;
import ru.tsc.kirillov.tm.util.GenericUtil;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public abstract class AbstractDtoService<M extends AbstractDtoModel, R extends IDtoRepository<M>>
        implements IDtoService<M> {

    @NotNull
    protected abstract IDtoRepository<M> getRepository();

    @NotNull
    protected final Class<M> modelClass = getModelClass();

    @NotNull
    @SneakyThrows
    protected Class<M> getModelClass() {
        return (Class<M>) GenericUtil.getClassFirst(getClass());
    }

    @NotNull
    @SneakyThrows
    protected M newInstance() {
        return modelClass.newInstance();
    }

    @Nullable
    protected M getResult(@NotNull final Page<M> page) {
        if (page.stream().count() == 0) return null;
        return page.getContent().get(0);
    }

    @NotNull
    protected Pageable getPageableForIndex(@NotNull final Integer index) {
        return PageRequest.of(index, 1);
    }

    @Nullable
    @Override
    public M add(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final IDtoRepository<M> repository = getRepository();
        return repository.save(model);
    }

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        @NotNull final IDtoRepository<M> repository = getRepository();
        return repository.saveAll(models);
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        @NotNull final IDtoRepository<M> repository = getRepository();
        repository.deleteAll();
        return repository.saveAll(models);
    }

    @Override
    public void clear() {
        @NotNull final IDtoRepository<M> repository = getRepository();
        repository.deleteAll();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final IDtoRepository<M> repository = getRepository();
        return repository.findAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        @NotNull final IDtoRepository<M> repository = getRepository();
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IDtoRepository<M> repository = getRepository();
        return repository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexOutOfBoundsException();
        @NotNull final IDtoRepository<M> repository = getRepository();
        return getResult(repository.findAll(getPageableForIndex(index)));
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final IDtoRepository<M> repository = getRepository();
        repository.delete(model);
        return model;
    }

    @Override
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        @NotNull final IDtoRepository<M> repository = getRepository();
        repository.deleteAll(collection);
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IDtoRepository<M> repository = getRepository();
        @Nullable final Optional<M> result = repository.findById(id);
        if (!result.isPresent()) throw new EntityNotFoundException();
        repository.deleteById(id);
        return result.get();
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexOutOfBoundsException();
        @NotNull final IDtoRepository<M> repository = getRepository();
        @Nullable final M model = findOneByIndex(index);
        if (model == null) return null;
        repository.delete(model);
        return model;
    }

    @Nullable
    @Override
    public M update(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final IDtoRepository<M> repository = getRepository();
        return repository.save(model);
    }


    @Override
    public long count() {
        @NotNull final IDtoRepository<M> repository = getRepository();
        return repository.count();
    }

}
