package ru.tsc.kirillov.tm.api.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.tsc.kirillov.tm.model.AbstractModel;

@NoRepositoryBean
public interface IRepository<M extends AbstractModel> extends JpaRepository<M, String> {

}
