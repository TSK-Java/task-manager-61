package ru.tsc.kirillov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import ru.tsc.kirillov.tm.dto.model.AbstractWbsDtoModel;

import java.util.List;

@NoRepositoryBean
public interface IWbsDtoRepository<M extends AbstractWbsDtoModel> extends IUserOwnedDtoRepository<M> {

    @NotNull
    List<M> findAll(@NotNull Sort sort);

    @NotNull
    List<M> findAllByUserId(@Nullable String userId, @NotNull Sort sort);

}
